# axolotl-aur-package

Here you can find an AUR package of Axolotl (https://github.com/nanu-c/axolotl) for Arch Linux and derivatives.

To use, simply execute `makepkg` from the root of this repository.
